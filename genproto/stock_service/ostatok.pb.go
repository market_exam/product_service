// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.6.1
// source: ostatok.proto

package stock_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type OstatokPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *OstatokPrimaryKey) Reset() {
	*x = OstatokPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ostatok_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *OstatokPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*OstatokPrimaryKey) ProtoMessage() {}

func (x *OstatokPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_ostatok_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use OstatokPrimaryKey.ProtoReflect.Descriptor instead.
func (*OstatokPrimaryKey) Descriptor() ([]byte, []int) {
	return file_ostatok_proto_rawDescGZIP(), []int{0}
}

func (x *OstatokPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type Ostatok struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ComingId    string  `protobuf:"bytes,2,opt,name=coming_id,json=comingId,proto3" json:"coming_id,omitempty"`
	CategoryId  string  `protobuf:"bytes,3,opt,name=category_id,json=categoryId,proto3" json:"category_id,omitempty"`
	ProductId   string  `protobuf:"bytes,4,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	Barcode     string  `protobuf:"bytes,5,opt,name=barcode,proto3" json:"barcode,omitempty"`
	Count       int32   `protobuf:"varint,6,opt,name=count,proto3" json:"count,omitempty"`
	ComingPrice float64 `protobuf:"fixed64,7,opt,name=coming_price,json=comingPrice,proto3" json:"coming_price,omitempty"`
}

func (x *Ostatok) Reset() {
	*x = Ostatok{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ostatok_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Ostatok) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Ostatok) ProtoMessage() {}

func (x *Ostatok) ProtoReflect() protoreflect.Message {
	mi := &file_ostatok_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Ostatok.ProtoReflect.Descriptor instead.
func (*Ostatok) Descriptor() ([]byte, []int) {
	return file_ostatok_proto_rawDescGZIP(), []int{1}
}

func (x *Ostatok) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Ostatok) GetComingId() string {
	if x != nil {
		return x.ComingId
	}
	return ""
}

func (x *Ostatok) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

func (x *Ostatok) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *Ostatok) GetBarcode() string {
	if x != nil {
		return x.Barcode
	}
	return ""
}

func (x *Ostatok) GetCount() int32 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *Ostatok) GetComingPrice() float64 {
	if x != nil {
		return x.ComingPrice
	}
	return 0
}

type CreateOstatok struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ComingId    string  `protobuf:"bytes,1,opt,name=coming_id,json=comingId,proto3" json:"coming_id,omitempty"`
	CategoryId  string  `protobuf:"bytes,2,opt,name=category_id,json=categoryId,proto3" json:"category_id,omitempty"`
	ProductId   string  `protobuf:"bytes,3,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	Barcode     string  `protobuf:"bytes,4,opt,name=barcode,proto3" json:"barcode,omitempty"`
	Count       int32   `protobuf:"varint,5,opt,name=count,proto3" json:"count,omitempty"`
	ComingPrice float64 `protobuf:"fixed64,6,opt,name=coming_price,json=comingPrice,proto3" json:"coming_price,omitempty"`
}

func (x *CreateOstatok) Reset() {
	*x = CreateOstatok{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ostatok_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateOstatok) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateOstatok) ProtoMessage() {}

func (x *CreateOstatok) ProtoReflect() protoreflect.Message {
	mi := &file_ostatok_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateOstatok.ProtoReflect.Descriptor instead.
func (*CreateOstatok) Descriptor() ([]byte, []int) {
	return file_ostatok_proto_rawDescGZIP(), []int{2}
}

func (x *CreateOstatok) GetComingId() string {
	if x != nil {
		return x.ComingId
	}
	return ""
}

func (x *CreateOstatok) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

func (x *CreateOstatok) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *CreateOstatok) GetBarcode() string {
	if x != nil {
		return x.Barcode
	}
	return ""
}

func (x *CreateOstatok) GetCount() int32 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *CreateOstatok) GetComingPrice() float64 {
	if x != nil {
		return x.ComingPrice
	}
	return 0
}

type UpdateOstatok struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ComingId    string  `protobuf:"bytes,2,opt,name=coming_id,json=comingId,proto3" json:"coming_id,omitempty"`
	CategoryId  string  `protobuf:"bytes,3,opt,name=category_id,json=categoryId,proto3" json:"category_id,omitempty"`
	ProductId   string  `protobuf:"bytes,4,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	Barcode     string  `protobuf:"bytes,5,opt,name=barcode,proto3" json:"barcode,omitempty"`
	Count       int32   `protobuf:"varint,6,opt,name=count,proto3" json:"count,omitempty"`
	ComingPrice float64 `protobuf:"fixed64,7,opt,name=coming_price,json=comingPrice,proto3" json:"coming_price,omitempty"`
}

func (x *UpdateOstatok) Reset() {
	*x = UpdateOstatok{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ostatok_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateOstatok) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateOstatok) ProtoMessage() {}

func (x *UpdateOstatok) ProtoReflect() protoreflect.Message {
	mi := &file_ostatok_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateOstatok.ProtoReflect.Descriptor instead.
func (*UpdateOstatok) Descriptor() ([]byte, []int) {
	return file_ostatok_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateOstatok) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateOstatok) GetComingId() string {
	if x != nil {
		return x.ComingId
	}
	return ""
}

func (x *UpdateOstatok) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

func (x *UpdateOstatok) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *UpdateOstatok) GetBarcode() string {
	if x != nil {
		return x.Barcode
	}
	return ""
}

func (x *UpdateOstatok) GetCount() int32 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *UpdateOstatok) GetComingPrice() float64 {
	if x != nil {
		return x.ComingPrice
	}
	return 0
}

type GetListOstatokRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetListOstatokRequest) Reset() {
	*x = GetListOstatokRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ostatok_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListOstatokRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListOstatokRequest) ProtoMessage() {}

func (x *GetListOstatokRequest) ProtoReflect() protoreflect.Message {
	mi := &file_ostatok_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListOstatokRequest.ProtoReflect.Descriptor instead.
func (*GetListOstatokRequest) Descriptor() ([]byte, []int) {
	return file_ostatok_proto_rawDescGZIP(), []int{4}
}

func (x *GetListOstatokRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListOstatokRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListOstatokRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type GetListOstatokResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ProductId     int64      `protobuf:"varint,1,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	StockProducts []*Ostatok `protobuf:"bytes,2,rep,name=stock_products,json=stockProducts,proto3" json:"stock_products,omitempty"`
}

func (x *GetListOstatokResponse) Reset() {
	*x = GetListOstatokResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ostatok_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListOstatokResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListOstatokResponse) ProtoMessage() {}

func (x *GetListOstatokResponse) ProtoReflect() protoreflect.Message {
	mi := &file_ostatok_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListOstatokResponse.ProtoReflect.Descriptor instead.
func (*GetListOstatokResponse) Descriptor() ([]byte, []int) {
	return file_ostatok_proto_rawDescGZIP(), []int{5}
}

func (x *GetListOstatokResponse) GetProductId() int64 {
	if x != nil {
		return x.ProductId
	}
	return 0
}

func (x *GetListOstatokResponse) GetStockProducts() []*Ostatok {
	if x != nil {
		return x.StockProducts
	}
	return nil
}

var File_ostatok_proto protoreflect.FileDescriptor

var file_ostatok_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x6f, 0x73, 0x74, 0x61, 0x74, 0x6f, 0x6b, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x0d, 0x73, 0x74, 0x6f, 0x63, 0x6b, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0x23,
	0x0a, 0x11, 0x4f, 0x73, 0x74, 0x61, 0x74, 0x6f, 0x6b, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79,
	0x4b, 0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x02, 0x69, 0x64, 0x22, 0xc9, 0x01, 0x0a, 0x07, 0x4f, 0x73, 0x74, 0x61, 0x74, 0x6f, 0x6b, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12,
	0x1b, 0x0a, 0x09, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b,
	0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0a, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64, 0x12, 0x1d, 0x0a,
	0x0a, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07,
	0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x62,
	0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x21, 0x0a, 0x0c,
	0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x01, 0x52, 0x0b, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x69, 0x63, 0x65, 0x22,
	0xbf, 0x01, 0x0a, 0x0d, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x4f, 0x73, 0x74, 0x61, 0x74, 0x6f,
	0x6b, 0x12, 0x1b, 0x0a, 0x09, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x5f, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x49, 0x64, 0x12, 0x1f,
	0x0a, 0x0b, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64, 0x12,
	0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x12, 0x18,
	0x0a, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e,
	0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x21,
	0x0a, 0x0c, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x06,
	0x20, 0x01, 0x28, 0x01, 0x52, 0x0b, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x69, 0x63,
	0x65, 0x22, 0xcf, 0x01, 0x0a, 0x0d, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x4f, 0x73, 0x74, 0x61,
	0x74, 0x6f, 0x6b, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x02, 0x69, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x5f, 0x69, 0x64,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x49, 0x64,
	0x12, 0x1f, 0x0a, 0x0b, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49,
	0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64,
	0x12, 0x18, 0x0a, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f,
	0x75, 0x6e, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74,
	0x12, 0x21, 0x0a, 0x0c, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65,
	0x18, 0x07, 0x20, 0x01, 0x28, 0x01, 0x52, 0x0b, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x50, 0x72,
	0x69, 0x63, 0x65, 0x22, 0x5d, 0x0a, 0x15, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x4f, 0x73,
	0x74, 0x61, 0x74, 0x6f, 0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06,
	0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66,
	0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x65,
	0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x61, 0x72,
	0x63, 0x68, 0x22, 0x76, 0x0a, 0x16, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x4f, 0x73, 0x74,
	0x61, 0x74, 0x6f, 0x6b, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x1d, 0x0a, 0x0a,
	0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x09, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x12, 0x3d, 0x0a, 0x0e, 0x73,
	0x74, 0x6f, 0x63, 0x6b, 0x5f, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x18, 0x02, 0x20,
	0x03, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x73, 0x74, 0x6f, 0x63, 0x6b, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x2e, 0x4f, 0x73, 0x74, 0x61, 0x74, 0x6f, 0x6b, 0x52, 0x0d, 0x73, 0x74, 0x6f,
	0x63, 0x6b, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x42, 0x18, 0x5a, 0x16, 0x67, 0x65,
	0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x73, 0x74, 0x6f, 0x63, 0x6b, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_ostatok_proto_rawDescOnce sync.Once
	file_ostatok_proto_rawDescData = file_ostatok_proto_rawDesc
)

func file_ostatok_proto_rawDescGZIP() []byte {
	file_ostatok_proto_rawDescOnce.Do(func() {
		file_ostatok_proto_rawDescData = protoimpl.X.CompressGZIP(file_ostatok_proto_rawDescData)
	})
	return file_ostatok_proto_rawDescData
}

var file_ostatok_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_ostatok_proto_goTypes = []interface{}{
	(*OstatokPrimaryKey)(nil),      // 0: stock_service.OstatokPrimaryKey
	(*Ostatok)(nil),                // 1: stock_service.Ostatok
	(*CreateOstatok)(nil),          // 2: stock_service.CreateOstatok
	(*UpdateOstatok)(nil),          // 3: stock_service.UpdateOstatok
	(*GetListOstatokRequest)(nil),  // 4: stock_service.GetListOstatokRequest
	(*GetListOstatokResponse)(nil), // 5: stock_service.GetListOstatokResponse
}
var file_ostatok_proto_depIdxs = []int32{
	1, // 0: stock_service.GetListOstatokResponse.stock_products:type_name -> stock_service.Ostatok
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_ostatok_proto_init() }
func file_ostatok_proto_init() {
	if File_ostatok_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_ostatok_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*OstatokPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ostatok_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Ostatok); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ostatok_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateOstatok); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ostatok_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateOstatok); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ostatok_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListOstatokRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ostatok_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListOstatokResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_ostatok_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_ostatok_proto_goTypes,
		DependencyIndexes: file_ostatok_proto_depIdxs,
		MessageInfos:      file_ostatok_proto_msgTypes,
	}.Build()
	File_ostatok_proto = out.File
	file_ostatok_proto_rawDesc = nil
	file_ostatok_proto_goTypes = nil
	file_ostatok_proto_depIdxs = nil
}
