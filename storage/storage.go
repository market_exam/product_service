package storage

import (
	"app/genproto/catalog_service"
	"context"
	// "app/models"
)

type StorageI interface {
	CloseDB()
	Category() CategoryRepoI
	Product() ProductRepoI
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.CategoryPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.Category, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateCategory) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) error
}

type ProductRepoI interface {
	Create(ctx context.Context, req *catalog_service.CreateProduct) (resp *catalog_service.ProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *catalog_service.ProductPrimaryKey) (resp *catalog_service.Product, err error)
	GetAll(ctx context.Context, req *catalog_service.GetListProductRequest) (resp *catalog_service.GetListProductResponse, err error)
	Update(ctx context.Context, req *catalog_service.UpdateProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *catalog_service.ProductPrimaryKey) error
}
