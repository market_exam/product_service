package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/genproto/catalog_service"
	"app/pkg/helper"
	"app/storage"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) storage.CategoryRepoI {
	return &CategoryRepo{
		db: db,
	}
}

func (c *CategoryRepo) Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.CategoryPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "categories" (
				id,
				parent_id,
				name,
				updated_at
			) VALUES ($1, $2, $3, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.ParentId,
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.CategoryPrimaryKey{Id: id.String()}, nil
}

func (c *CategoryRepo) GetByPKey(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.Category, err error) {

	query := `
		SELECT
			id,
			parent_id,
			name,
			created_at,
			updated_at,
			deleted_at
		FROM "categories"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id         sql.NullString
		parent_id  sql.NullString
		name       sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
		deleteddAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&parent_id,
		&name,
		&createdAt,
		&updatedAt,
		&deleteddAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Category{
		Id:        id.String,
		ParentId:  parent_id.String,
		Name:      name.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
		DeletedAt: deleteddAt.String,
	}

	return
}

func (c *CategoryRepo) GetAll(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error) {

	resp = &catalog_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			COALESCE(parent_id, ''),
			name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "categories"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var category catalog_service.Category

		err := rows.Scan(
			&resp.Count,
			&category.Id,
			&category.ParentId,
			&category.Name,
			&category.CreatedAt,
			&category.UpdatedAt,
			&category.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Categories = append(resp.Categories, &category)
	}

	return
}

func (c *CategoryRepo) Update(ctx context.Context, req *catalog_service.UpdateCategory) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "categories"
			SET
				parent_id = :parent_id,
				name      = :name,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"parent_id": req.GetParentId(),
		"name":      req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *CategoryRepo) Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) error {

	query := `DELETE FROM "categories" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
