package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/genproto/catalog_service"
	"app/pkg/helper"
	"app/storage"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.ProductRepoI {
	return &ProductRepo{
		db: db,
	}
}

func (c *ProductRepo) Create(ctx context.Context, req *catalog_service.CreateProduct) (resp *catalog_service.ProductPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "products" (
				id,
				category_id,
				product_name,
				price,
				photo,
				barcode,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.CategoryId,
		req.ProductName,
		req.Price,
		req.Photo,
		req.Barcode,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.ProductPrimaryKey{Id: id.String()}, nil
}

func (c *ProductRepo) GetByPKey(ctx context.Context, req *catalog_service.ProductPrimaryKey) (resp *catalog_service.Product, err error) {

	query := `
		SELECT
			id,
			category_id,
			product_name,
			price,
			photo,
			barcode,
			created_at,
			updated_at,
			deleted_at
		FROM "products"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id           sql.NullString
		category_id  sql.NullString
		product_name sql.NullString
		price        sql.NullFloat64
		photo        sql.NullString
		barcode      sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
		deleteddAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&category_id,
		&product_name,
		&price,
		&photo,
		&barcode,
		&createdAt,
		&updatedAt,
		&deleteddAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Product{
		Id:          id.String,
		CategoryId:  category_id.String,
		ProductName: product_name.String,
		Price:       price.Float64,
		Photo:       photo.String,
		Barcode:     barcode.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
		DeletedAt:   deleteddAt.String,
	}

	return
}

func (c *ProductRepo) GetAll(ctx context.Context, req *catalog_service.GetListProductRequest) (resp *catalog_service.GetListProductResponse, err error) {

	resp = &catalog_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	var category_id sql.NullString

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			category_id,
			product_name,
			COALESCE(price, 0),
			photo,
			barcode,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "products"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		filter = " AND category_id ILIKE " + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var product catalog_service.Product

		err := rows.Scan(
			&resp.Count,
			&product.Id,
			&product.CategoryId,
			&product.ProductName,
			&product.Price,
			&product.Photo,
			&product.Barcode,
			&product.CreatedAt,
			&product.UpdatedAt,
			&product.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		product.CategoryId = category_id.String

		resp.Products = append(resp.Products, &product)
	}

	return
}

func (c *ProductRepo) Update(ctx context.Context, req *catalog_service.UpdateProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "products"
			SET
				category_id  = :category_id,
				product_name = :product_name,
				price        = :price,
				photo        = :photo,
				barcode      = :barcode,
				updated_at   = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"category_id":  req.GetCategoryId(),
		"product_name": req.GetProductName(),
		"price":        req.GetPrice(),
		"photo":        req.GetPhoto(),
		"barcode":      req.GetBarcode(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ProductRepo) Delete(ctx context.Context, req *catalog_service.ProductPrimaryKey) error {

	query := `DELETE FROM "products" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
