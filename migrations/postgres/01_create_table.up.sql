
CREATE TABLE IF NOT EXISTS "categories" (
    "id"          UUID PRIMARY KEY,
    "parent_id"   VARCHAR(50),
    "name"        VARCHAR(50) NOT NULL,
    "created_at"  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMP,
    "deleted_at"  TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "products" (
    "id"          UUID PRIMARY KEY,
    "category_id" UUID NOT NULL REFERENCES categories (id),
    "product_name" VARCHAR(50) NOT NULL,
    "price"       NUMERIC,
    "photo"       VARCHAR(50) NOT NULL,
    "barcode"     VARCHAR(50) NOT NULL,
    "created_at"  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMP,
    "deleted_at"  TIMESTAMP
);

SELECT
	COUNT(*) OVER(),
	id,
	parent_id,
	name,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "categories"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 

SELECT
	COUNT(*) OVER(),
	id,
	category_id,
	product_name,
	COALESCE(price, 0),
	photo,
    barcode,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "products"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 
