// ORGANIZATION SERVICE

TABLE filials {
  "id"    UUID [PRIMARY KEY]
  "filial_code"  VARCHAR
  "name"         VARCHAR
  "address"      VARCHAR
  "phone_number" VARCHAR
  "created_at"   TIMESTAMP
  "updated_at"   TIMESTAMP
  "deleted_at"   TIMESTAMP
}

TABLE sale_points {
  "id"  UUID [PRIMARY KEY]
  "filial_id"    UUID
  "name"         VARCHAR
  "created_at"   TIMESTAMP
  "updated_at"   TIMESTAMP
  "deleted_at"   TIMESTAMP
}

TABLE employees {
  "id"     UUID [PRIMARY KEY]
  "filial_id"    UUID
  "sales_id"     UUID
  "first_name"   VARCHAR
  "last_name"    VARCHAR
  "phone_number" VARCHAR
  "created_at"   TIMESTAMP
  "updated_at"   TIMESTAMP
  "deleted_at"   TIMESTAMP
}

TABLE curiers {
  "id"     UUID [PRIMARY KEY]
  "first_name"   VARCHAR
  "last_name"    VARCHAR
  "phone_number" VARCHAR
  "active"       BOOL
  "created_at"   TIMESTAMP
  "updated_at"   TIMESTAMP
  "deleted_at"   TIMESTAMP
}

// PRODUCT SERVICE

TABLE categories {
 "id" UUID [PRIMARY KEY]
 "parent_id" UUID
 "name" VARCHAR
 "created_at" TIMESTAMP
 "updated_at" TIMESTAMP
 "deleted_at" TIMESTAMP
}

TABLE products {
  "id"  UUID [PRIMARY KEY]
  "category_id" VARCHAR
  "name"        VARCHAR
  "price"       NUMERIC
  "photo"       VARCHAR
  "barcode"     VARCHAR
  "created_at"  TIMESTAMP
  "updated_at"  TIMESTAMP
  "deleted_at"  TIMESTAMP
}

// STOCK SERVICE 

TABLE comings {
  "id" UUID [PRIMARY KEY]
  "filial_id" UUID
  "curier_id" UUID
  "date"       TIMESTAMP
  "status"     VARCHAR
  "created_at" TIMESTAMP
  "updated_at" TIMESTAMP
  "deleted_at" TIMESTAMP
}

TABLE coming_products {
  "id"   UUID [PRIMARY KEY]
  "coming_id"    UUID
  "category_id"  UUID
  "product_id"   UUID
  "barcode"      VARCHAR
  "count"        INT
  "coming_price" NUMERIC
  "created_at"   TIMESTAMP
  "updated_at"   TIMESTAMP
  "deleted_at"   TIMESTAMP
}

TABLE ostatok {
  "id" UUID [PRIMARY KEY]
  "coming_id" UUID
  "filial_id" UUID
  "category_id"  UUID
  "product_id"   UUID
  "barcode"      VARCHAR
  "coming_price" NUMERIC
  "count"        INT
  "created_at"   TIMESTAMP
  "updated_at"   TIMESTAMP
  "deleted_at"   TIMESTAMP
}

// KASSA SERVICE

TABLE sale {
  "id" UUID [PRIMARY KEY]
  "smena_id"        UUID
  "filial_id"       UUID
  "sale_point_id"   UUID
  "employee_id"     UUID
  "status"          VARCHAR
  "created_at"      TIMESTAMP
  "updated_at"      TIMESTAMP
  "deleted_at"      TIMESTAMP
}

TABLE sale_products {
  "id" UUID [PRIMARY KEY]
  "sale_id" UUID
  "category_id" UUID
  "product_id" UUID
  "filial_id"  UUID
  "barcode" VARCHAR
  "ostatok_count" INT
  "count" INT
  "price_without_discount" NUMERIC
  "discount" NUMERIC
  "discount_type" VARCHAR 
  "price" NUMERIC
  "total_price" NUMERIC
  "created_at" TIMESTAMP
  "updated_at" TIMESTAMP
  "deleted_at" TIMESTAMP
}

TABLE payment {
   "id" UUID [PRIMARY KEY]
   "sale_id" UUID 
   "cash" NUMERIC
   "uzcard" NUMERIC
   "humo" NUMERIC
   "apelsin" NUMERIC
   "payme" NUMERIC
   "click" NUMERIC
   "total_summa" NUMERIC
   "created_at" TIMESTAMP
   "updated_at" TIMESTAMP
   "deleted_at" TIMESTAMP
}

TABLE smena {
  "id" UUID [PRIMARY KEY]
  "filial_id"      UUID
  "employee_id"    UUID
  "sale_points_id" UUID
  "status"         VARCHAR
  "created_at"     TIMESTAMP
  "updated_at"     TIMESTAMP
  "deleted_at"     TIMESTAMP
}

TABLE transaction {
   "id" UUID [PRIMARY KEY]
   "smena_id" UUID 
   "cash" NUMERIC
   "uzcard" NUMERIC
   "humo" NUMERIC
   "apelsin" NUMERIC
   "payme" NUMERIC
   "click" NUMERIC
   "total_summa" NUMERIC
   "created_at" TIMESTAMP
   "updated_at" TIMESTAMP
   "deleted_at" TIMESTAMP
}


REF: products.category_id > categories.id

REF: transaction.smena_id > smena.id

REF: payment.sale_id > sale.id

REF: sale_points.filial_id > filials.id

REF: sale.smena_id > smena.id

REF: coming_products.coming_id > comings.id

REF: employees.filial_id > filials.id

REF: sale_products.sale_id < sale.id

REF: ostatok.coming_id < comings.id





