package service

import (
	"context"
	"fmt"

	// "fmt"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"app/config"
	"app/genproto/catalog_service"

	"app/grpc/client"
	"app/pkg/logger"
	"app/storage"
)

type CategoryService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedCategoryServiceServer
}

func NewCategoryService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *CategoryService {
	return &CategoryService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *CategoryService) Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.Category, err error) {

	i.log.Info("---CreateCategory------>", logger.Any("req", req))

	pKey, err := i.strg.Category().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateCategory->Category->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Category().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyCategory->Category->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *CategoryService) GetById(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.Category, err error) {

	fmt.Println("salom")

	c.log.Info("---GetCategoryByID------>", logger.Any("req", req))

	resp, err = c.strg.Category().GetByPKey(ctx, req)
	if err != nil {
		c.log.Error("!!!GetCategoryByID->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// if len(resp.ParentId) > 0{

	// 	resp.ParentData, err = c.strg.Category().GetByPKey(ctx, &catalog_service.CategoryPrimaryKey{CategoryId: resp.ParentCategoryId})
	// 	if err != nil {
	// 	c.log.Error("!!!GetCategoryByID->Get ParentData->Get--->", logger.Error(err))
	// 	return nil, status.Error(codes.InvalidArgument, err.Error())
	// 	}

	// }

	return
}

func (i *CategoryService) GetList(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error) {

	i.log.Info("---GetCategories------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategories->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// for ind, _ := range resp.Categories {

	// 	if len(resp.Categories[ind].ParentCategoryId) > 0{

	// 		resp.Categories[ind].ParentData, err = i.strg.Category().GetByPKey(ctx, &catalog_service.CategoryPrimaryKey{CategoryId: resp.Categories[ind].ParentCategoryId})
	// 		if err != nil {
	// 		i.log.Error("!!!GetCategoryList->Get ParentData->Get--->", logger.Error(err))
	// 		return nil, status.Error(codes.InvalidArgument, err.Error())
	// 		}

	// 	}

	// }

	return
}

func (i *CategoryService) Update(ctx context.Context, req *catalog_service.UpdateCategory) (resp *catalog_service.Category, err error) {

	i.log.Info("---UpdateCategory------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Category().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Category().GetByPKey(ctx, &catalog_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *CategoryService) Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteCategory------>", logger.Any("req", req))

	err = i.strg.Category().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
