package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"app/config"
	"app/genproto/catalog_service"
	"app/grpc/client"
	"app/grpc/service"
	"app/pkg/logger"
	"app/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	catalog_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))
	catalog_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
